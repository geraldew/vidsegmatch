#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# vidsegmatch = video segment match
#
# --------------------------------------------------
# Copyright (C) 2021-2021  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
#import os as im_os
#import os.path as osp

import tkinter as im_tkntr 
import tkinter.ttk as im_tkntr_ttk
#import tkinter.scrolledtext as im_tkntr_tkst
from tkinter import scrolledtext as im_tkntr_tkst 
from tkinter import filedialog as im_filedialog 
from tkinter import messagebox as im_messagebox

import pathlib as im_pathlib

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

# --------------------------------------------------
# Global scope objects
# --------------------------------------------------

# This needs to be here, not for time priority but for global scope
# global thismodule_log 

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def app_code_name():
	return "vidsegmatch"

def app_code_abbr():
	return "vdsgmtch"

def app_show_name():
	return "Vidsegmatch"

def app_code_abbr_s():
	return app_code_abbr() + " "

def app_version_code_show() :
	return "v0.0.1"

def app_show_name_version() :
	return app_show_name() + " " + app_version_code_show()

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# --------------------------------------------------
# Class extentions - tkinter
# --------------------------------------------------

# --------------------------------------------------
# Custom Settings Supports
# --------------------------------------------------

# --------------------------------------------------
# Log controls
# --------------------------------------------------

# --------------------------------------------------
# Interactive
# --------------------------------------------------

def getpathref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a Folder"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_foldref = im_filedialog.askdirectory( title=show_title, initialdir=from_folder_name)
	return i_foldref

def getloadfileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Load from"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_fileref = im_filedialog.askopenfilename( title=show_title, initialdir=from_folder_name)
	return i_fileref

def getsavefileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Save to"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_fileref = im_filedialog.asksaveasfilename( title=show_title, initialdir=from_folder_name)
	return i_fileref

def ApplicationWindow( p_window_context ) : 
	# Brief:
	# Provide all the details of the user interface 
	# Parameters:
	# - p_window_context - a tkinter parent reference
	# Returns:
	# Notes:
	# the code structure used here is to first have all the sub "def"s for the element actions
	# then define all the elements
	# As there is zero likelihood of wanting multiple instances, it is NOT written as a Class
	# the concept being implemented is a single window with a notebook of multiple tabs
	# ------------
	# Code layout:
	# - Feature defs
	# - - all tabs
	# - - per tab
	# - GUI action defs
	# - - all tabs
	# - - per tab
	# - GUI definition block
	# - - create Notebook of tabs
	# - - per tab elements, usually as multiple frames
	# ------------
	# Feature defs
	def bool_to_chkboxvar( p_bool):
		if p_bool :
			v = 1
		else :
			v = 0
		return v
	def colour_bg_danger():
		return "plum1"
	def colour_bg_safely():
		return "PaleGreen1"
	def colour_bg_config():
		return "orange"
	def not_running_else_say( str_action ) :
		# concept here is to return whether we're free to run a process and if not to show a message about it
		if process_running :
			# Choice between using showinfo, showwarning and showerror
			im_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	# ------------
	# GUI interaction defs
	# For Tab 1
	def on_button_exit() :
		# protect from exiting while a process is running for when is asynchronous
		if not_running_else_say( "Exiting" ) :
			p_window_context.quit()
	# For Tab 2
	def t2_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if tb2_lock_ctrls :
			im_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	# File A
	def t2_on_button_browse_file_a() :
		if t2_not_running_else_say( "Browsing for File A" ) :
			got_path = t2_var_file_a.get()
			got_string = getloadfileref_fromuser( "Select File A", got_path)
			t2_var_file_a.set( got_string)
	# File B
	def t2_on_button_browse_file_b() :
		if t2_not_running_else_say( "Browsing for File B" ) :
			got_path = t2_var_file_b.get()
			got_string = getloadfileref_fromuser( "Select File B", got_path)
			t2_var_file_b.set( got_string)
	# File B
	def t2_on_button_browse_path_tmp() :
		if t2_not_running_else_say( "Browsing for Temp path" ) :
			got_path = t2_var_path_tmp.get()
			got_string = getpathref_fromuser( "Select Path", got_path)
			t2_var_path_tmp.set( got_string)
	def t2_on_button_action() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal process_running
		button_action_txt = "Run Action !"
		if process_running :
			if t2_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t2_label_actionise_status.configure( text = "Abandoned processing in mode:")
				process_running = False
				t2_button_actionise_text.set( button_action_txt )
		elif True :
			t2_label_actionise_status.configure( text = "Actioning..." + " mode:")
			process_running = True
			t2_button_actionise_text.set("Halt Action !")
			i_str_file_a = t2_var_file_a.get()
			i_str_file_b = t2_var_file_b.get()
			i_str_path_tmp = t2_var_path_tmp.get()
			if ( len(i_str_file_a) > 0 ) and ( len(i_str_file_b) > 0 ) and ( len(i_str_path_tmp) > 0 ) :
				# action here
				str_outcome = "No outcome:"
				im_tkntr.messagebox.showwarning(app_show_name(), str_outcome) 
			else :
				im_tkntr.messagebox.showwarning(app_show_name(), "At least one path is empty!") 
			# return GUI to normal
			process_running = False
			t2_label_actionise_status.configure( text = "Run Terminated" )
			t2_button_actionise_text.set( button_action_txt )
		else :
			im_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a problem somewhere!") 
			t2_label_actionise_status.configure( text = "No Run. There is a clash somewhere!" )
	# For Tab 5 GUI Process Log
	def t5_ProcessLog_AddLine( st ):
		st_plogs.insert(im_tkntr.END, '\n' + cmntry.hndy_time_prefix() + " " + st)
		print( st)
	# -------------------------------------------------
	# main for ApplicationWindow
	process_running = False
	# --------- Frame 0 Title
	# Have a top section outside the Tabbed interface
	frame_top = im_tkntr.Frame( p_window_context)
	frame_top.pack( fill = im_tkntr.X)
	# make frame content
	button_exit = im_tkntr.Button( frame_top, text="(X)", command=on_button_exit)
	label_title = im_tkntr.Label( frame_top, text=app_show_name_version() )
	label_explain = im_tkntr.Label( frame_top, text="A tool for matching segments of videos.")
	# place the content
	button_exit.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	label_title.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	label_explain.pack( side = im_tkntr.RIGHT, padx=5, pady=5)
	# Make the notebook i.e. tabbed interface
	nb = im_tkntr_ttk.Notebook( p_window_context)
	nb.pack( expand=1, fill=im_tkntr.BOTH)
	# ========== Make 1st tab = Reserved
	tab_1 = im_tkntr.Frame(nb)
	nb.add( tab_1, text="Reserved")
	tb1_lock_ctrls = False
	# ========== Make and add the 2nd tab 
	tab_2 = im_tkntr.Frame(nb)
	nb.add( tab_2, text="Match Two Files")
	tb2_lock_ctrls = False
	# --------- Frame 1 File A
	t2_frame1 = im_tkntr.Frame( tab_2, bg=colour_bg_safely())
	t2_frame1.pack( fill = im_tkntr.X)
	t2_label_browse_file_a = im_tkntr.Label( t2_frame1, text="File A", bg=colour_bg_safely())
	t2_var_file_a = im_tkntr.StringVar()
	t2_entry_file_a = im_tkntr.Entry( t2_frame1, textvariable=t2_var_file_a)
	t2_button_browse_file_a = im_tkntr.Button( t2_frame1, text="Browse..", command=t2_on_button_browse_file_a)
	#  layout
	t2_label_browse_file_a.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_entry_file_a.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1, padx=5, pady=5) 
	t2_button_browse_file_a.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	# --------- Frame 2 File B
	t2_frame2 = im_tkntr.Frame( tab_2, bg=colour_bg_safely())
	t2_frame2.pack( fill = im_tkntr.X)
	t2_label_browse_file_b = im_tkntr.Label( t2_frame2, text="File B", bg=colour_bg_safely())
	t2_var_file_b = im_tkntr.StringVar()
	t2_entry_file_b = im_tkntr.Entry( t2_frame2, textvariable=t2_var_file_b)
	t2_button_browse_file_b = im_tkntr.Button( t2_frame2, text="Browse..", command=t2_on_button_browse_file_b)
	#  layout
	t2_label_browse_file_b.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_entry_file_b.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1, padx=5, pady=5) 
	t2_button_browse_file_b.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	# --------- Frame 3 Controls Line 1
	t2_frame3 = im_tkntr.Frame( tab_2)
	t2_frame3.pack( fill = im_tkntr.X)
	t2_label_browse_path_tmp = im_tkntr.Label( t2_frame3, text="Temp path", bg=colour_bg_safely())
	t2_var_path_tmp = im_tkntr.StringVar()
	t2_entry_path_tmp = im_tkntr.Entry( t2_frame3, textvariable=t2_var_path_tmp)
	t2_button_browse_path_tmp = im_tkntr.Button( t2_frame3, text="Browse..", command=t2_on_button_browse_path_tmp)
	#  layout
	t2_label_browse_path_tmp.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_entry_path_tmp.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1, padx=5, pady=5) 
	t2_button_browse_path_tmp.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	# --------- Frame 4 Controls Line 2
	t2_frame4 = im_tkntr.Frame( tab_2)
	t2_frame4.pack( fill = im_tkntr.X)
	# --------- Frame 5 Controls Line 3
	t2_frame5 = im_tkntr.Frame( tab_2)
	t2_frame5.pack( fill = im_tkntr.X)
	# --------- Frame 8 Action buttons
	
	t2_frame8 = im_tkntr.Frame( tab_2)
	t2_frame8.pack( fill = im_tkntr.X)
	#
	t2_button_actionise_text = im_tkntr.StringVar()
	t2_button_actionise_text.set("Run Action !")
	t2_button_actionise = im_tkntr.Button( t2_frame8, textvariable=t2_button_actionise_text, command=t2_on_button_action)
	t2_label_actionise_status = im_tkntr.Label( t2_frame8, text="_", fg = "blue") # , width=60
	#
	t2_button_actionise.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	t2_label_actionise_status.pack( side=im_tkntr.RIGHT )
	# ========== Make and add the 5th tab = Process Logs
	tab_5 = im_tkntr.Frame(nb)
	nb.add( tab_5, text="Process Logs")
	# ----------
	st_plogs = im_tkntr_tkst.ScrolledText( tab_5)
	st_plogs.pack( side=im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=True )
	st_plogs.insert(im_tkntr.INSERT, "GUI logging enabled.")
	#st_plogs.insert(im_tkntr.END, " in ScrolledText")
	# ========== Make and add the 6th tab = Results
	tab_6 = im_tkntr.Frame(nb)
	nb.add( tab_6, text="Results")
	# ----------
	st_rslts = im_tkntr_tkst.ScrolledText( tab_6)
	st_rslts.pack( side=im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=True )
	st_rslts.insert(im_tkntr.INSERT, "No results yet!")

def main_gui_interact( ) :
	# Brief:
	# Enact the graphic user interface, allow the user set values and then launch actions.
	# Parameters:
	# Returns:
	# Notes:
	# this acts as a parent holder for the GUI as a window form with tkinter functions
	# Code:
	# setup the log controls
	# prep and launch the GUI
	root = im_tkntr.Tk()
	root.title(app_show_name())
	root.wm_geometry("780x500")
	# define the main window with tkinter features
	ApplicationWindow( root )
	# get the GUI in action
	root.mainloop()

def setup_logging() :
	#  this is currently just a placeholder for where the logging setup will be called
	return False

def command_line_actions( loadfilename ) :
	#  this is currently just a placeholder for where the command line action will be coded
	print( "command_line_actions")

def handle_command_line( lst_arg ) :
	# this is currently just framework code for deciding whether we're doing a command line or GUI
	# all that matters here is that 
	# gui_mode gets returned as True until we know what command line functions we want to have
	# at which point we'll come back to here and do clever stuff to enable that
	if __debug__:
		pass
	gui_mode = False
	loadfilename = ""
	if __debug__:
		pass
	for arg in lst_arg :
		if __debug__:
			pass
	some_action = False
	if not some_action :
		gui_mode = True
	if some_action and len(loadfilename) > 0 :
		if __debug__:
			pass
		command_line_actions( loadfilename,  ) 
		pass
	return gui_mode, loadfilename

# main to execute only if run as a program
if __name__ == "__main__":
	if __debug__:
		pass
	# command line parameter checking
	# later do something smarter
	# e.g. if enough info is passed and/or indication to avoid the GUI
	# quite a few things to consider: e.g. allow pass parameters as presetting but then use the GUI
	# currently that logic is all in the "old" main_command_line which is lying fallow while the
	# GUI approach is being worked out in full, to in turn discover all the desired features
	arg_count = len(im_sys.argv)
	if arg_count < 2 :
		# in effect no parameters as the first/zeroth parameter is the command itself
		gui_mode = True
		preloadfilename = ""
	else:
		if __debug__:
			pass
		gui_mode, preloadfilename = handle_command_line( im_sys.argv ) 
	if gui_mode :
		if __debug__:
			pass
		main_gui_interact( ) # later make pass preloadfilename through for pickup into the GUI mode
	if __debug__:
		pass
