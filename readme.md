# Vidsegmatch

An intended tool for content similarity checking of video files.

At time of writing there is no program, only an intention. This document therefore, is an attempt to describe that intention.

## Origin and Requirement

Here is the circumstance from which this need arose.

I use a PVR device with a USB hard drive for time-shifting of broadcsat TV shows. As a device, it provides no editing features, and indeed there is no method of organising the videos during their life between recordings and deletion (i.e. after having been watched). However, I am able to plug the hard drive into my computer, and thereby use an editor there to remove the surplus recording before and after the desired programme. FWIW I use a program called VidCutter on Linux.

Note: I have no desire to keep these long-term, this is all about time-shifting, watching, then deleting the episodes - and I have no interest in removing the advertisements.

For one show that I watch this way, the TV station has shown a confusing mixture of new episodes and repeats. As it is shown five nights a week, there can quickly become a queue of episode recordings, of which I have already seen some but some that I have not.

Indeed, it is clear that the repeats include many repetitions.

Among the catches of this, is that performing the edits, while very useful to reduce the space the recordings take place, also has the effect of removing my ability to "spot" which ones I've already seen. Why? Beause I _have_ seen little bits during the edit process.

Thanks to a change in the on-stage set of this particular show, I have split them into "old set" and "new set" groups, wherein, I am much more likely to have fully watched those of the old set. However, this is also a catch, as due to that very probability I have a backlog of "old set" episodes that I haven't edited - which means those video files begin with end parts of the preceding shows from the time of recording (and similarly with the beginning of the following programme).

Thus I have a paradox about the multiple "old set" files - as I'm very likely to have already seen them, they don't seem worth the effort of editing, especially not until I've watched the "new set" episodes. However, doing any form of looking for multiple holdings will be much less efficient until they have been edited.

Oh, ultimately the "have I seen it" test will be done by the rest of the household, when time is free to actually watch an episode - as they have not been affected by the editing like I have. Although, the annoyance of watching one enough to realise that it has already been seen, is now a drag on enthusiasm for watching at all.

So what I seem to need, is a program that can trawl through a large number of video files and work out which ones contain the same episodes.

Additional thought. When these videos are of TV with advertisements, the "ads" are themselves an inherent cause of content repetition. Therefore, assuming it is feasible to take fingerprints of sections of video, it _should_ be expected that such pieces will complicate the problem of recognising the programme content from amongst the ads.

## Method Thoughts

We could start by having a technique for comparing two videos.

An initial pass:
- Go through one, making a number of image grabs with a constant frequency
- Go through the second one, checking every frame for matches with the earlier grabs

Thus we end up with a set of:
- Grabs of A - each with time and image
- Correspondence points in B - each with time and reference to item of A

This gives us sets of A - B correspondences as data points.
- If we find none at all, then we are done.
- Where we find some, what do we do?

Well, we could work in either direction from each correspondence point and produce mappings of matching time ranges. Where such extensions from correspondence touch each other they get combined into a single range.

Imagine that we find a match between:
- 05:00 in A, with 19:43 in B

and then in scanning both backwards we get them being matches until 3:35 in A, and 18:18 in B

and scanning forwards instead we get them being matches until 16:43 in A, and 31:26 in B - then this produces a data row looking like:

| A_from | A_upto | B_from | B_upto |
| ------ | ------ | ------ | ------ |
| 3:35   | 16:43  | 18:18  | 31:26  |

- etc

We now have a data interpretation problem rather than a video analysis one.

The above was reasoned somewhat on the assumption of a linear program with no internal repetitions to throw us off. But video which includes adverts may also cause matches even if the programme material is distinct. This begs the question of how we tell which is which.  

If we know A has been trimmed to be just the programme then we might be able to make some deductions about the parts of B that match. Notably, if we therefore know that the beginning of A is not an advert, then we also know that for the matching part of B.

Once we can seed an ability to know which matching parts are programme and which are adverts, we can consider making a bank of adverts (e.g. as extracts of some kind) against which to compare.

## Signatures and Fingerprints

For things other than video it seems common to consider analysing and then storing "signatures" or "fingerprints" of file material. The above was written to avoid these terms to keep the brief open to methods that either or do not use such an appraoch.

But, separately, it is a good question to ponder if these are useful ideas in an implementation. Either way, it's probably worth exploring what those concepts would mean when applied to video files. 

## Programming Thoughts

As Python 3 is my current goto coding language, Python it will be for the initial attempt.

### Features Required

- Ability to navigate folders and files - `import os`
- Ability to process a video file and extract frames
- Ability to process a video file looking for frame matches
- Ability to do a fuzzy match of two images
- Ability to store matches as data - `import sqlite3`

